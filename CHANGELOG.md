## [1.11.1](https://bitbucket.org/limelighthealth/stern-auth-schema/compare/1.11.0...1.11.1) (2020-12-17)


### Bug Fixes

* small updates for docker image ([1ef907d](https://bitbucket.org/limelighthealth/stern-auth-schema/commits/1ef907da5e355064f0a4a095cfcd80546791fce8))

# [1.11.0](https://bitbucket.org/limelighthealth/stern-auth-schema/compare/1.10.0...1.11.0) (2020-12-17)


### Features

* cleanup after cloning plans and rates schema ([c00a61f](https://bitbucket.org/limelighthealth/stern-auth-schema/commits/c00a61f17c1ebea2738c0088b1dcae7216e74f42))
