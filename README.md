# Stern auth DB Schema #

## Description ##
This repository contains the schema for the Stern auth database. The schema will be packaged up into a docker image that will contain the schema, liquibase for database version control and also scripts to run the liquibase changelogs. The image can be used to run against an already existing database or a newly created database. The image will be used to manage and push database configurations to the Stern auth database used by [Quotepad](https://bitbucket.org/limelighthealth/quotepad/).

## Requirements ##
Git, NVM, Yarn, Node and Docker

## Setup Environment ##
1. [Install NVM](https://itnext.io/nvm-the-easiest-way-to-switch-node-js-environments-on-your-machine-in-a-flash-17babb7d5f1b)
2. Run this command: `nvm install 12`
3. Run this command `nvm use 12` (nvm use allows you to easily jump between node versions)
4. *Optional* you can set your default Node version to be used by running the command `nvm alias default 12`
5. [Install YARN](https://classic.yarnpkg.com/en/docs/install/)
6. [Install Docker](https://docs.docker.com/get-docker/)
7. Run this command `yarn install`

You should now have the project setup and ready to configure for use
   
## Configure your `.env`
1. Run this command `cp .env.template .env`
The `.env` file contains all the configuration needed to run a Stern auth DB Schema image against a database. Go ahead and update those environment variables to the configuration you would like.

## Build an image ##
Once you have your configuration set you are ready to build your schema images. The `package.json` file in the root of the project provides some scripts to help you build your image
1. To build an image run the command `yarn run build`

## Run an image (Local testing)##
In the root of the project there is a docker-compose file that can be used to test out running your newly created image. The docker-compose file contains a service for starting up a postgres database. The image for the postgres database will use the configuration from your `.env` file. To start up your services simply run `yarn run compose` and the services will start up. Once the services have finished running(plans_and_rates_db_liquibase has exited) then you can login locally to the postgres database and check the configuration of the database, for this you can use tools such as [navicat](https://www.navicat.com/en/) or [postico](https://eggerapps.at/postico/).

## Tear down services ##
1. To tear down services kill the docker containers running by pressing `Ctrl+C` in the terminal that the services are running.
2. Once that is done you can run `yarn run clean` and that will remove the volumes. 

Additional cleanup:
If you want to cleanup your entire system of docker images etc.. you can run
1. `docker system prune`
2. `docker volume prune`

## How it works ##
1. When the docker image is run it first runs the `wait-for-it.sh` script which checks if the target database is up and running (your `.env` informs liquibase as to what database should be the target)
2. Next the `liquibase-init.sh` script runs to setup the liquibase configuration
3. The image has an env variable set for `LIQUIBASE_CHANGELOG` (See Dockerfile) which tells liquibase what changelog to run and that changelog lists the child changelogs to be run. Each file is then run in sequential order as listed in the changelog against the database with the provide user.

## How versioning works ##
How versioning works is pretty simple and is all explained in this [How Liquibase Works](https://www.youtube.com/watch?v=5AnCHzVa_7o) video.

## Contributing ## 
Before making any changes please review the Contributing guidelines [Code Review Guidelines](./CONTRIBUTING.md)
1. It should be noted that changes here will be made to all implementation databases when the implementation takes the version update so keep that in mind when adding schema changes
2. This repository deals only with schema and does not seed any data.
3. Changes should not be made to older scripts and instead new scripts should be added to implement the required changes.