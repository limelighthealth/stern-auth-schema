## Commit Message Guidelines

We follow the [Conventional-Commits specification](https://www.conventionalcommits.org/), this leads to **more
readable messages** that are easy to follow and also
allows to automate process like **semver releases and the changelog**.

When committing changes, a git precommit hook will run, where the message format is validated.

### Commit Message Format
Each commit message consists of a **header**, a **body (optional)** and a **footer** (optional).  The header has a special
format that includes a **type**, a **scope** and a **subject**:

```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

### Example commit-messages:

#### New Feature
```
feat(events): add new table for events (#DBE-1004)
```

#### Bug fix
```
fix(events): update event id column to string (#DBE-1840)
```

#### Refactor Existing Feature
```
refactor(events): changing of events to be read only (#DBE-1840)
```

#### Refactor - with `!` suffix (BREAKING CHANGE)
```
refactor!(events): rename events table (#PLAT-9999)
```

#### Improving documentation (no reference to ticket)
```
docs: correct spelling of README
```

### Commit types

| Commit Type | Title                    | Description                                                                                                 | Emoji  |
| ----------- | ------------------------ | ----------------------------------------------------------------------------------------------------------- |:------:|
| `feat`      | Features                 | A new feature                                                                                               | ✨     |
| `fix`       | Bug Fixes                | A bug Fix                                                                                                   | 🐛     |
| `docs`      | Documentation            | Documentation only changes                                                                                  | 📚     |
| `style`     | Styles                   | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)      | 💎     |
| `refactor`  | Code Refactoring         | A code change that neither fixes a bug nor adds a feature                                                   | 📦     |
| `perf`      | Performance Improvements | A code change that improves performance                                                                     | 🚀     |
| `test`      | Tests                    | Adding missing tests or correcting existing tests                                                           | 🚨     |
| `build`     | Builds                   | Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)         | 🛠     |
| `ci`        | Continuous Integrations  | Changes to our CI configuration files and scripts (example scopes: Travis, Circle, BrowserStack, SauceLabs) | ⚙️     |
| `chore`     | Chores                   | Other changes that don't modify src or test files                                                           | ♻️     |
| `revert`    | Reverts                  | Reverts a previous commit                                 

Commit types originally from:
* [Angular Git Commit Message Conventions](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#type)

A detailed explanation can be found in the [Conventional-Commits website](https://www.conventionalcommits.org/).

See more examples in [Angular project](https://github.com/angular/angular/commits/master).


## Versioning of repository
The repository stores its version in the `package.json` file in the root of the project. This version is automatically updated via bitbucket pipelines based on the commit message. When merging PR's you must squash the commits(this can be done in bitbucket UI) and follow the commit message guidelines. Images are then created via jenkins and pushed to Amazon container registry.