--liquibase formatted sql

--changeset vsuvorov:1

/*
 PostgreSQL database dump

 Dumped from database version 9.6.11
 Dumped by pg_dump version 12.0

 Source:
  Host     : rainbow.ct5efpnnxk2h.us-west-2.rds.amazonaws.com:5432
  Database : qa-stern
  Schema   : auth

 Started on 2020-02-21 11:16:02 PST
*/
--echo-all

SET SESSION AUTHORIZATION 'postgres';

------------
-- CLEANUP
------------
DROP SCHEMA IF EXISTS "auth" CASCADE;
DROP SCHEMA IF EXISTS "changeset" CASCADE;


----------------
-- SCHEMA AUTH
----------------
-- Name: auth; Type: SCHEMA; Schema: -; Owner: postgres
CREATE SCHEMA IF NOT EXISTS "auth";

----------
-- TABLE
----------
-- Name: users; Type: TABLE; Schema: auth; Owner: postgres
CREATE TABLE "auth"."users" (
    "id" integer NOT NULL,
    "email" "text" NOT NULL,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);

------------
-- SEQUENCE
------------
-- Name: users_id_seq; Type: SEQUENCE; Schema: auth; Owner: postgres
CREATE SEQUENCE "auth"."users_id_seq"
    START WITH 11
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE "auth"."users_id_seq" OWNED BY "auth"."users"."id";


-------------------
-- DEFAULT ID VALUES
-------------------
-- Name: users id; Type: DEFAULT; Schema: auth; Owner: postgres
ALTER TABLE ONLY "auth"."users" ALTER COLUMN "id" SET DEFAULT "nextval"('"auth"."users_id_seq"'::"regclass");


----------------
-- CONSTRAINTS
----------------
-- Name: users users_email; Type: CONSTRAINT; Schema: auth; Owner: postgres
ALTER TABLE ONLY "auth"."users"
    ADD CONSTRAINT "users_email" UNIQUE ("email");


-- Name: users users_pkey; Type: CONSTRAINT; Schema: auth; Owner: postgres
ALTER TABLE ONLY "auth"."users"
    ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");


/*
 PostgreSQL database dump

 Dumped from database version 9.6.11
 Dumped by pg_dump version 12.0

 Source:
  Host     : rainbow.ct5efpnnxk2h.us-west-2.rds.amazonaws.com:5432
  Database : qa-stern
  Schema   : changeset

 Started on 2020-02-20 11:54:10 PST
*/

--------------------
-- SCHEMA CHANGESET
--------------------
-- Name: changeset; Type: SCHEMA; Schema: -; Owner: postgres
CREATE SCHEMA IF NOT EXISTS "changeset";


----------
-- TYPE
----------
-- Name: change_type; Type: TYPE; Schema: changeset; Owner: postgres
CREATE TYPE "changeset"."change_type" AS ENUM (
    'UPSERT',
    'DELETE'
);
SET default_tablespace = '';

-----------
-- TABLES
-----------
-- Name: change_logs; Type: TABLE; Schema: changeset; Owner: postgres
CREATE TABLE "changeset"."change_logs" (
    "id" integer NOT NULL,
    "changeset_id" integer NOT NULL,
    "environment" "text" NOT NULL,
    "logged_at" timestamp without time zone DEFAULT "now"() NOT NULL,
    "author_id" integer NOT NULL,
    "is_rollback" boolean DEFAULT false NOT NULL,
    "comment" "text" DEFAULT ''::"text" NOT NULL
);

-- Name: changes; Type: TABLE; Schema: changeset; Owner: postgres
CREATE TABLE "changeset"."changes" (
    "id" integer NOT NULL,
    "created_at" timestamp without time zone DEFAULT "now"() NOT NULL,
    "author_id" integer NOT NULL,
    "changeset_id" integer NOT NULL,
    "model" "text" NOT NULL,
    "type" "changeset"."change_type" NOT NULL,
    "record_primary_key" "jsonb" NOT NULL,
    "commit_patch" "jsonb",
    "rollback_patch" "jsonb",
    "comment" "text" DEFAULT ''::"text" NOT NULL,
    "is_active" boolean DEFAULT true NOT NULL
);

-- Name: changesets; Type: TABLE; Schema: changeset; Owner: postgres
CREATE TABLE "changeset"."changesets" (
    "id" integer NOT NULL,
    "created_at" timestamp without time zone DEFAULT "now"() NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "author_id" integer NOT NULL,
    "comment" "text" DEFAULT ''::"text" NOT NULL,
    "is_abandoned" boolean DEFAULT false NOT NULL
);


-----------
-- VIEWS
-----------
-- Name: change_environments; Type: VIEW; Schema: changeset; Owner: postgres
CREATE VIEW "changeset"."change_environments" AS
 SELECT "changes"."id",
    "changes"."created_at",
    "changes"."author_id",
    "changes"."changeset_id",
    "changes"."model",
    "changes"."type",
    "changes"."record_primary_key",
    "changes"."commit_patch",
    "changes"."rollback_patch",
    "changes"."comment",
    "changes"."is_active",
    ( SELECT "array_agg"("e"."environment") AS "environments"
           FROM ( SELECT "change_logs"."environment"
                   FROM "changeset"."change_logs"
                  WHERE (("change_logs"."changeset_id" = "changes"."changeset_id") AND (NOT "change_logs"."is_rollback"))
                EXCEPT ALL
                 SELECT "change_logs"."environment"
                   FROM "changeset"."change_logs"
                  WHERE (("change_logs"."changeset_id" = "changes"."changeset_id") AND "change_logs"."is_rollback")) "e") AS "environments"
   FROM "changeset"."changes";

-- Name: changeset_environments; Type: VIEW; Schema: changeset; Owner: postgres
CREATE VIEW "changeset"."changeset_environments" AS
 SELECT "changesets"."id",
    "changesets"."created_at",
    "changesets"."updated_at",
    "changesets"."author_id",
    "changesets"."comment",
    "changesets"."is_abandoned",
    ( SELECT "array_agg"("e"."environment") AS "array_agg"
           FROM ( SELECT "change_logs"."environment"
                   FROM "changeset"."change_logs"
                  WHERE (("change_logs"."changeset_id" = "changesets"."id") AND (NOT "change_logs"."is_rollback"))
                EXCEPT ALL
                 SELECT "change_logs"."environment"
                   FROM "changeset"."change_logs"
                  WHERE (("change_logs"."changeset_id" = "changesets"."id") AND "change_logs"."is_rollback")) "e") AS "environments"
   FROM "changeset"."changesets";

-- Name: changesets_by_quoter; Type: VIEW; Schema: changeset; Owner: postgres
CREATE VIEW "changeset"."changesets_by_quoter" AS
 SELECT "count"("x"."record_primary_key") AS "count",
    "x"."author_id",
    "x"."email",
    "x"."changeset_id",
    "x"."changeset_name",
    ("x"."quoter_id")::"text" AS "quoter_id",
    "l"."environment",
    "max"("l"."logged_at") AS "max_logged_at",
    "l"."is_rollback"
   FROM (( SELECT "c"."author_id",
            "u"."email",
            "c"."changeset_id",
            "s"."comment" AS "changeset_name",
            "c"."model",
            "c"."type",
            "c"."record_primary_key",
            "c"."commit_patch",
            ("json_array_elements"(("c"."commit_patch")::"json") -> 'path'::"text") AS "path",
            ("json_array_elements"(("c"."commit_patch")::"json") -> 'value'::"text") AS "quoter_id"
           FROM (("changeset"."changes" "c"
             JOIN "changeset"."changesets" "s" ON (("s"."id" = "c"."changeset_id")))
             JOIN "auth"."users" "u" ON (("c"."author_id" = "u"."id")))
          WHERE ("c"."model" ~~ 'operations'::"text")) "x"
     JOIN "changeset"."change_logs" "l" ON (("x"."changeset_id" = "l"."changeset_id")))
  WHERE (("x"."path")::"text" ~~ '"/quoter_id"'::"text")
  GROUP BY "x"."author_id", "x"."email", "x"."changeset_id", ("x"."quoter_id")::"text", "x"."changeset_name", "l"."environment", "l"."is_rollback"
  ORDER BY ("x"."quoter_id")::"text", "x"."changeset_id", ("max"("l"."logged_at"));


--------------
-- SEQUENCES
--------------
-- Name: change_logs_id_seq; Type: SEQUENCE; Schema: changeset; Owner: postgres
CREATE SEQUENCE "changeset"."change_logs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    -- The defaults are 1 and 2^63-1
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE "changeset"."change_logs_id_seq" OWNED BY "changeset"."change_logs"."id";

-- Name: changes_id_seq; Type: SEQUENCE; Schema: changeset; Owner: postgres
CREATE SEQUENCE "changeset"."changes_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE "changeset"."changes_id_seq" OWNED BY "changeset"."changes"."id";

-- Name: changesets_id_seq; Type: SEQUENCE; Schema: changeset; Owner: postgres
CREATE SEQUENCE "changeset"."changesets_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE "changeset"."changesets_id_seq" OWNED BY "changeset"."changesets"."id";


-------------------
-- DEFAULT ID VALUES
-------------------
-- Name: change_logs id; Type: DEFAULT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."change_logs" ALTER COLUMN "id" SET DEFAULT "nextval"('"changeset"."change_logs_id_seq"'::"regclass");

--
-- Name: changes id; Type: DEFAULT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."changes" ALTER COLUMN "id" SET DEFAULT "nextval"('"changeset"."changes_id_seq"'::"regclass");

--
-- Name: changesets id; Type: DEFAULT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."changesets" ALTER COLUMN "id" SET DEFAULT "nextval"('"changeset"."changesets_id_seq"'::"regclass");


----------------
-- CONSTRAINTS
----------------
-- Name: change_logs change_logs_pkey; Type: CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."change_logs"
    ADD CONSTRAINT "change_logs_pkey" PRIMARY KEY ("id");

--
-- Name: changes changes_pkey; Type: CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."changes"
    ADD CONSTRAINT "changes_pkey" PRIMARY KEY ("id");

--
-- Name: changesets changesets_pkey; Type: CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."changesets"
    ADD CONSTRAINT "changesets_pkey" PRIMARY KEY ("id");

--
-- Name: changes no_simultaneous_changes; Type: CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."changes"
    ADD CONSTRAINT "no_simultaneous_changes" UNIQUE ("created_at", "record_primary_key");


----------
-- INDEX
----------
-- Name: changeset_id; Type: INDEX; Schema: changeset; Owner: postgres

CREATE INDEX "changeset_id" ON "changeset"."changes" USING "btree" ("changeset_id");


----------------------------
-- FOREIGN KEY CONSTRAINTS
----------------------------
-- Name: change_logs change_logs_author_id_fkey; Type: FK CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."change_logs"
    ADD CONSTRAINT "change_logs_author_id_fkey" FOREIGN KEY ("author_id") REFERENCES "auth"."users"("id");

--
-- Name: change_logs change_logs_changeset_id_fkey; Type: FK CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."change_logs"
    ADD CONSTRAINT "change_logs_changeset_id_fkey" FOREIGN KEY ("changeset_id") REFERENCES "changeset"."changesets"("id");

--
-- Name: changes changes_author_id_fkey; Type: FK CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."changes"
    ADD CONSTRAINT "changes_author_id_fkey" FOREIGN KEY ("author_id") REFERENCES "auth"."users"("id");

--
-- Name: changes changes_changeset_id_fkey; Type: FK CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."changes"
    ADD CONSTRAINT "changes_changeset_id_fkey" FOREIGN KEY ("changeset_id") REFERENCES "changeset"."changesets"("id");

--
-- Name: changesets changesets_author_id_fkey; Type: FK CONSTRAINT; Schema: changeset; Owner: postgres

ALTER TABLE ONLY "changeset"."changesets"
    ADD CONSTRAINT "changesets_author_id_fkey" FOREIGN KEY ("author_id") REFERENCES "auth"."users"("id");
