--liquibase formatted sql

--changeset vsuvorov:2
INSERT INTO "auth"."users" VALUES (1, 'vladislav@limelighthealth.com', '2020-02-02 00:00:00', '2020-02-02 00:00:00');
INSERT INTO "auth"."users" VALUES (2, 'jagan.reddy@limelighthealth.com', '2020-02-02 00:00:00', '2020-02-02 00:00:00');
INSERT INTO "auth"."users" VALUES (3, 'colin.ray@limelighthealth.com', '2020-02-02 00:00:00', '2020-02-02 00:00:00');
INSERT INTO "auth"."users" VALUES (4, 'michael.stirchak@limelighthealth.com', '2020-02-02 00:00:00', '2020-02-02 00:00:00');
