def COMMIT_AUTHOR
def PACKAGE_VERSION
pipeline {
    triggers {
        pollSCM 'H/45 * * * *'
    }

    agent {
        node{
            label 'ec2-fleet'
        }
    }
    options {
        durabilityHint 'MAX_SURVIVABILITY'
        parallelsAlwaysFailFast()
        skipStagesAfterUnstable()
        warnError('Unstable build')
        timeout(60)
    }
    environment {
        DOCKER_BUILDKIT='1'
        AWS_ECR_URL = credentials('aws_ecr_url')
        ECR_ROLE_ACCOUNT = credentials('ecr_role_account')
        AWS_ECR_REGION = credentials('aws_ecr_region')
    }
    stages {
        stage('Configure Job'){
             steps {
                script {
                    COMMIT_AUTHOR = sh(returnStdout: true, script: 'git --no-pager show -s --format="%an" $GIT_COMMIT').trim()
                    PACKAGE_VERSION = sh(returnStdout: true, script: 'awk -F\'"\' \'/"version": ".+"/{ print $4; exit; }\' package.json').trim()
                    DOCKER_LABEL = env.GIT_COMMIT + env.BUILD_NUMBER
                    BRANCH_NAME = sh(returnStdout: true, script: 'echo $GIT_BRANCH | sed -e "s|.*/||g"').trim()
                    BRANCH_TAG = BRANCH_NAME + '-latest'
                }
            }
        }
        stage('Build Image'){
            when{
                beforeAgent true
                expression {
                    return COMMIT_AUTHOR == 'semantic-release-bot';
                }
            }
            steps {
                withAWS(role: 'jenkins', roleAccount: "$ECR_ROLE_ACCOUNT", roleSessionName: 'jenkins-session') {
                    sh '''
                        set +x
                        docker logout
                        aws ecr get-login-password --region $AWS_ECR_REGION | docker login --username AWS --password-stdin $AWS_ECR_URL
                        set -x
                    '''
                    sh 'docker build --label ' + DOCKER_LABEL + ' -t stern_auth_schema:' + PACKAGE_VERSION +' .'
                    sh "docker tag stern_auth_schema:" + PACKAGE_VERSION + "  $AWS_ECR_URL/stern_auth_schema:$PACKAGE_VERSION"
                    sh "docker push $AWS_ECR_URL/stern_auth_schema:$PACKAGE_VERSION"
                    
                    script {
                        echo 'Building from branch:' + BRANCH_NAME
                        if (BRANCH_NAME == 'master') {
                            BRANCH_TAG = 'latest'
                        }
                        sh "docker tag stern_auth_schema:" + PACKAGE_VERSION + "  $AWS_ECR_URL/stern_auth_schema:" + BRANCH_TAG
                        sh "docker push $AWS_ECR_URL/stern_auth_schema:$BRANCH_TAG"
                    }
                }
            }
        }
    }
    post {
        unstable {
            slackSend (channel: 'continuous-integration', color: 'good', message: 'Stern Auth Schema ' + PACKAGE_VERSION + ': Unstable Build  (<' + env.BUILD_URL + '>)')
        }
        success {
            slackSend (channel: 'continuous-integration', color: 'good', message: 'Stern Auth Schema ' + PACKAGE_VERSION + ': Successful Build  (<' + env.BUILD_URL + '>)')
        }
        failure {
            slackSend (channel: 'continuous-integration', color: 'danger', message: 'Stern Auth Schema: Failed Build  (<' + env.BUILD_URL + '>)')
        }
        cleanup {
            sh 'docker builder prune --filter=label=' + DOCKER_LABEL + ' --all --force'
            sh 'docker system prune  --filter=label=' + DOCKER_LABEL + ' --all --force --volumes'
        }
    }
}
